#include "mock.h"

namespace mock {

constexpr const char* MOCK_TYPE[] =
{
  "ARRAY", // from postgres
  "BOOL",  // from postgres
  "BIGINT",
  "BINARY",
  "BIT",
  "CHAR",
  "DATE",
  "DATETIME",
  "DECIMAL",
  "FLOAT",
  "GEOGRAPHY",
  "GEOMETRY",
  "HIERARCHYID",
  "IMAGE",
  "INT",
  "INTERNAL", // from postgres
  "MONEY",
  "NCHAR",
  "NTEXT",
  "NUMERIC",
  "NVARCHAR",
  "REAL",
  "SMALLDATETIME",
  "SMALLINT",
  "SMALLMONEY",
  "SQL_VARIANT",
  "TEXT",
  "TIME",
  "TIMESTAMP",
  "TINYINT",
  "UNIQUEIDENTIFIER",
  "VARBINARY",
  "VARCHAR",
  "XML",
};

constexpr int MOCK_TABLE_SIZE = 3;
constexpr int MOCK_COLUMN_SIZE = 3;

constexpr const char* MOCK_TABLE[MOCK_TABLE_SIZE] =
{
  "student",
  "course",
  "enrollment",
};

constexpr const char* MOCK_COLUMN[][MOCK_COLUMN_SIZE][2] =
{
  // table `student`
  {
    { "sid",  "UNIQUEIDENTIFIER" },
    { "name", "VARCHAR" },
    { "age",  "INT" }
  },
  // table `course`
  {
    { "cid",   "UNIQUEIDENTIFIER" },
    { "name",  "VARCHAR" },
    { "tutor", "VARCHAR" },
  },
  // table `enrollment`
  {
    { "sid",  "UNIQUEIDENTIFIER" },
    { "cid",  "UNIQUEIDENTIFIER" },
    { "date", "DATETIME" },
  },
};

constexpr const char* MOCK_OPERATORS[][4] =
{
  { "<",  "INT",   "INT",   "BOOL" },
  { ">",  "INT",   "INT",   "BOOL" },
  { "<=", "INT",   "INT",   "BOOL" },
  { ">=", "INT",   "INT",   "BOOL" },
  { "==", "INT",   "INT",   "BOOL" },
  { "!=", "INT",   "INT",   "BOOL" },
  { "<",  "VCHAR", "VCHAR", "BOOL" },
  { ">",  "VCHAR", "VCHAR", "BOOL" },
  { "<=", "VCHAR", "VCHAR", "BOOL" },
  { ">=", "VCHAR", "VCHAR", "BOOL" },
  { "==", "VCHAR", "VCHAR", "BOOL" },
  { "!=", "VCHAR", "VCHAR", "BOOL" },
};

// simulate a statically structured graph graph for now
// TODO make it flexible later
mock_schema::mock_schema()
  : schema()
{
  // types
  for (const auto & t: MOCK_TYPE)
    types.push_back(sqltype::get(t));

  booltype = sqltype::get("BOOL");
  inttype  = sqltype::get("INT");
  internaltype = sqltype::get("INTERNAL");
  arraytype = sqltype::get("ARRAY");

  // tables
  for (auto i = 0; i < MOCK_TABLE_SIZE; ++i)
  {
    table t(MOCK_TABLE[i], "mock_catalog", /* insertable */ false, /* base_table */ true);
    for (auto j = 0; j < MOCK_COLUMN_SIZE; ++j)
    {
      auto name = MOCK_COLUMN[i][j][0];
      auto type = sqltype::get(MOCK_COLUMN[i][j][1]);
      t.columns().emplace_back(name, type);
    }
    tables.push_back(t);
  }

  // operators
  for (auto opdef : MOCK_OPERATORS)
  {
    const auto name = opdef[0];
    const auto left = sqltype::get(opdef[1]);
    const auto right = sqltype::get(opdef[2]);
    const auto result = sqltype::get(opdef[3]);
    register_operator(op(name, left, right, result));
  }

  // OK, that's enough for now

  generate_indexes();
}

}

