#ifndef MOCK_H
#define MOCK_H

#include <string>

#include "relmodel.hh"
#include "schema.hh"

namespace mock {

struct mock_sqltype : sqltype {};

struct mock_schema : schema
{
  mock_schema();
  virtual std::string quote_name(const std::string&) { return "mock_schema"; }
};

}

#endif // MOCK_H

